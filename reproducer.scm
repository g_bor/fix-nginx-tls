(use-modules (guix scripts substitute)
	     (srfi srfi-1)
	     (srfi srfi-26)
	     (web uri)
	     (web request)
	     (web response)
	     (rnrs io ports))

(define http-mulitple-get
  (@@ (guix scripts substitute) http-multiple-get))

(define %base-url "https://localhost")

(define %request-count 500)

(http-mulitple-get (string->uri %base-url)
		   (lambda (request response port result)
		     (let ((len (or (response-content-length response)
				    0)))
		       (pk 'resp (length result)
			   (uri-path (request-uri request)))
		       (get-bytevector-n port len)
		       (cons result result)))
		   '()
		   (unfold (cut >= <> %request-count)
			   (lambda (n)
			     (build-request
			      (string->uri
			       (string-append
				%base-url
				"/"
				(string-pad (number->string n) 32 #\a)
				".narinfo"))
			      #:method 'GET
			      #:headers '((User-Agent . "GNU Guile"))))
			   1+
			   0)
		   #:verify-certificate? #f)
