(use-modules (guix packages)
	     (guix hg-download)
	     (guix utils)
	     (gnu packages web))

(define nginx-good
  (package
   (inherit nginx)
   (version "1.17.4")
   (source (origin
	    (method hg-fetch)
	    (uri (hg-reference
		  (url "https://hg.nginx.org/nginx")
		  (changeset "efd71d49bde0")))
	    (sha256
	     (base32
	      "1fk8dwxnjw6ijmaifqbs0c6adxf4bnfgg4alr2mp6fk7salj228z"))))
   (arguments
    (substitute-keyword-arguments (package-arguments nginx)
				  ((#:phases phases)
				   `(modify-phases ,phases
						   (add-after 'unpack 'symlink-confgiure
							      (lambda _
								(symlink "auto/configure" "configure")
								#t))))))))
				   

nginx-good
