(use-modules (guix packages)
	     (guix download)
	     (gnu packages web))

(define nginx-1.17.5
  (package
   (inherit nginx)
   (version "1.17.5")
   (source (origin
	    (method url-fetch)
	    (uri (string-append "https://nginx.org/download/nginx-"
				version ".tar.gz"))
	    (sha256
	     (base32
      	      ;"0mg521bxh8pysmy20x599m252ici9w97kk7qy7s0wrv6bqv4p1b2"))))))
	      "1hqhziic4csci8xs4q8vbzpmj2qjkhmmx68zza7h5bvmbbhkbvk3"))))))

nginx-1.17.5
