(use-modules (guix packages)
	     (guix hg-download)
	     (guix utils)
	     (gnu packages web))

(define nginx-bad
  (package
   (inherit nginx)
   (version "1.17.4")
   (source (origin
	    (method hg-fetch)
	    (uri (hg-reference
		  (url "https://hg.nginx.org/nginx")
		  (changeset "9d2ad2fb4423")))
	    (sha256
	     (base32
	      "0sss962nnn8yaagwvn7j2m0vmzw1xq643rakcy1hd50aczn62pgp"))))
   (arguments
    (substitute-keyword-arguments (package-arguments nginx)
				  ((#:phases phases)
				   `(modify-phases ,phases
						   (add-after 'unpack 'symlink-configure
							      (lambda _
								(symlink "auto/configure" "configure")
								#t))))))))
				   

nginx-bad
